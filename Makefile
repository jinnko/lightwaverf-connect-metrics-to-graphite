
.PHONY: install
install:
	echo "You will be prompted for your sudo password now in order to install the cron config to /etc/cron.d/lightwaverf"
	echo "*/15 * * * * $(USER) $(PWD)/lightwaverf.zsh | nc -q0 $(shell awk -F= '/CARBON_HOST/ {print $$2}' config) $(shell awk -F= '/CARBON_PORT/ {print $$2}' config)" | sudo tee /etc/cron.d/lightwaverf
