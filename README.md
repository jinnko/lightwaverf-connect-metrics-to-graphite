
# LightwaveRF Connect Metrics to Graphite

A tool to authenticate to the LightwaveRF Connect Manager web service and scrape data from the dashboard.

## Requirements

- curl
- make
- zsh

## Configuration

Using `config-sample` as a basis create your own `config`, and protect it by only allowsing read and write by your own user.

    cp config-sample config
    chmod 0600 config

To populate the configuration you'll need to use a web browser to capture some details.  This is done as follows:

### Getting the login credentials

1. Open https://manager.lightwaverf.com
2. In Firefox press CTRL-SHIFT-E to get the Network Inspector.
3. Enter your crednetials and log in
4. In the Network Inspector:
   1. Enter `login.php` into the filter box and click on the request.
   2. Click on `Params` on the right.
   3. From the `Request paylod` box, copy the `username` and `password` values into your local configuration file.

### Getting the MAC and Rooms

#### MAC ID

1. Log into your Lighwave RF Connect account
2. Navigate to "Lightwaver RF Link" from your profile menu at the top right.
3. The MAC is the last three pairs of hexidecimal digits.  When you fill that into the config file
   you must replace the ':' (colon) characters with '%3A' (i.e. URL encode it).

#### Rooms

1. Log into your Lighwave RF Connect account
2. Navigate to a view where you see the devices listed.
3. When you hover over the devices you'll see the Serial for the given device in the hover details
   at the top right.
4. When you fill that into the config file you must replace the ':' (colon) characters with '%3A'
   (i.e. URL encode it).


NOTE: You'll need to copy the URL encoded strings, similar to the examples provided in the `config-sample` file.

### Installation

    make install

This will be prompted for your sudo password in order to install to `/etc/cron.d/lightwaverf`

## Contributions

Contributions are welcome.  Fork the repository and raise a pull request with your changes.
