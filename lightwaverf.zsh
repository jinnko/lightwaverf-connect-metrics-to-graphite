#!/usr/bin/env zsh

set -euo pipefail

source "$(dirname "$0")/config"

# Check if we're logged in first by trying to view the favs
if ! curl -s https://manager.lightwaverf.com/favs --cookie "$COOKIE_PATH" | grep '^<body ng-app="webManager" data-env="prod">' >/dev/null; then
  # Seems we're not logged in, lets get a session first
  curl -s https://manager.lightwaverf.com/ \
    --location \
    --cookie-jar "$COOKIE_PATH" \
    --compressed \
    -o /dev/null
  # Now login with the session
  curl -s 'https://manager.lightwaverf.com/process/login.php' \
    --location \
    --cookie "$COOKIE_PATH" \
    --compressed \
    -o /dev/null\
    -H 'Content-Type: application/x-www-form-urlencoded' \
    --data 'username='"$USERNAME"'&password='"$PASSWORD"'&submit=login'
fi

# Metrics are pulled using this request
TEMPLATE="curl -s https://manager.lightwaverf.com/process/get-datadashboard.php -H Accept:application/json --compressed -H Content-Type:application/x-www-form-urlencoded --cookie $COOKIE_PATH --data dataString=https%3A%2F%2Fconsole.lightwaverf.com%2Fapi%2Fv2%2Fdashboard%2Ftemp%3Fmac%3D$MAC%26serial%3DSERIAL%26from%3DFROM%26to%3DTO%26limit%3D5000"
YESTERDAY=$(date -d yesterday +'%d%%2F%m%%2F%Y')
REQUEST_TMPL=${TEMPLATE/FROM/$YESTERDAY}
TODAY=$(date +'%d%%2F%m%%2F%Y')
REQUEST_TMPL=${REQUEST_TMPL/TO/$TODAY}

for room serial in ${(kv)ROOMS}; do
  REQUEST=${REQUEST_TMPL/SERIAL/$serial}
  ${=REQUEST} \
    | jq -r '.rows[] | "current_target \(.current_target) \(.time)\nnext_target \(.next_target) \(.time)\noutput \(.output) \(.time)\ncurrent_temp \(.current_temp) \(.time)"' \
    | sed 's/^/lightwaverf.'"$COLLECTD_IDENTIFIER"'.'$room'./'
done
